'use strict';
const date = require('hof').components.date;

module.exports = {
  'given-name': {
    validate: 'required'
  },
  'family-name': {
    validate: 'required'
  },
  'date-of-birth': date('date-of-birth', {
    validate: ['required', 'before']
  }),
  'ihs-number': {
    validate: 'required'
  },
  'address-line-1': {
    validate: 'required'
  },
  'address-line-2': {},
  'town': {
    validate: 'required'
  },
  'postcode': {
    validate: ['required', 'postcode']
  }
};
