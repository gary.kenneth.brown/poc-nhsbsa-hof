'use strict';

const hof = require('hof');
const config = require('./config');

let appName = 'IHS2';

let settings = require('./hof.settings');

settings = Object.assign({}, settings, {
  //behaviours: settings.behaviours.map(require),
  routes: settings.routes.map(require),
  redis: config.redis,
  path: "/assets"
});

const app = hof(settings);

app.use((req, res, next) => {
  // Set HTML Language
  res.locals.htmlLang = 'en';
  res.locals.appName = appName;
  // Set feedback and footer links
  res.locals.feedbackUrl = '/feedback';
  res.locals.footerSupportLinks = [
    { path: '/contact', property: 'Contact' },
    { path: '/cookies', property: 'Cookies' },
    { path: '/privacy-policy', property: 'Privacy Policy' },
    { path: '/accessibility-statement', property: 'Accessibility Statement' }
  ];
  next();
});

module.exports = app;
